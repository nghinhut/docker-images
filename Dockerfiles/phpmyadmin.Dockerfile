FROM phpmyadmin/phpmyadmin:latest

RUN touch /etc/nginx.conf
RUN echo "/docker-entrypoint.sh" > /run.sh && chmod +x /run.sh
