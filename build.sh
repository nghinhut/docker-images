function buildChanges {
    # Diff last with latest commit, list dir name, fillter duplicate name
    for Dockerfile in $(git diff @~1 @ --name-only | xargs dirname | grep -E "^[a-z0-9-]+$" | sort -u); do
        buildImage ${Dockerfile}
    done
}
